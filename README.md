## MCS

- `_depen`: dependencies between invariants.
- `mcs.cafe`: specification.
- `inv1`, `inv2`,..., `inv7`: proof scores for corresponding invariant/lemma.
– `others` folder: proof scores for other invariants that do not need for proof of mutex of MCS such as inv50, inv51, etc.
- `tas.cafe`: specification, proof scores of the simple mutual exclusion protocol - TAS.
- `cimpa` folder [IGNORE]: contains the proof scripts for CiMPA (written manually).
- `cimpg` folder [IGNORE]: contains the annotated proof scores and the generated proof scripts by CiMPG.